package simpleserver;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Map;
import simpleserver.DataMap.Post;
import simpleserver.Response.JsonBuilder;


public class SimpleServer {

  private static SimpleServer server;
  private DataMap map;

  private SimpleServer() {

  }

  private static SimpleServer getInstance() {
    if (server == null) {
      server = new SimpleServer();
    }
    return server;
  }

  /**
   * Opens the socket and handles both receiving and sending packets.
   * @throws IOException Throws an IOException if no data file is found
   **/
  private void run() throws IOException {
    //initialize the data map
    map = new DataMap();
    ServerSocket ding;
    Socket dong = null;
    PrintWriter writer;
    try {
      ding = new ServerSocket(1299);
      System.out.println("Opened socket " + 1299);
      while (true) {

        // keeps listening for new clients, one at a time
        try {
          dong = ding.accept(); // waits for client here
        } catch (IOException e) {
          System.out.println("Error opening socket");
          System.exit(1);
        }

        InputStream stream = dong.getInputStream();
        BufferedReader in = new BufferedReader(new InputStreamReader(stream));

        BufferedOutputStream out = new BufferedOutputStream(dong.getOutputStream());
        writer = new PrintWriter(out, true);  // char output to the client
        try {

          // read the first line to get the request method, URI and HTTP version
          String line = in.readLine();
          String url = line;

          System.out.println("----------REQUEST START---------");
          System.out.println(line);

          // read only headers
          line = in.readLine();
          while (line != null && line.trim().length() > 0) {
            int index = line.indexOf(": ");
            if (index > 0) {
              System.out.println(line);
            } else {
              break;
            }
            line = in.readLine();
          }

          String output = processUrl(url);
          writer.println(output);
          System.out.println(output);

          System.out.println("----------REQUEST END---------\n\n");
        } catch (IOException e) {
          System.out.println("Error reading");
          System.exit(1);
        }

        dong.close();
      }
    } catch (IOException e) {
      System.out.println("Error opening socket");
      System.exit(1);
    }
  }

  /**
   * Processes the URL and returns a string in Json format to print.
   * @param url A string URL used as the
   * @return A string in Json format is returned
   **/
  private String processUrl(String url) {
    String status;
    int entries = 0;
    String output = null;
    url = url.split("/")[1];
    System.out.println(url);
    String[] urlArray = url.split("\\?");
    String endpoint = urlArray[0];
    //remove the /
    String param1 = endpoint;
    System.out.println(param1);
    param1 = endpoint;
    String param2 = "";
    if (urlArray.length > 1) {
      param2 = urlArray[1].toLowerCase();
      param2 = param2.substring(0, param2.length() - 5);
      System.out.println(param2);
      param2 = param2.trim();
    }
    if (param2.equals("") || Character.isDigit(param2.charAt(param2.length() - 1))) {

      System.out.println(param1);
      System.out.println(param2);
      //To make sure everything worked okay

      int n;
      switch (param1) {
        case "user":
          output = map.getUsers().toString();
          status = "OK";
          entries = map.getUserNumber();
          break;
        case "posts":

          if (!param2.equals("") && param2.substring(0, param2.length() - 2).equals("userid")) {
            status = "Error: Your second parameter was invalid. Please try again";
            break;
          }

          //The last char in the parameter should be the number**
          n = Character.getNumericValue(param2.charAt(param2.length() - 1));
          if (map.getPosts(n) != null) {

            entries = map.getPostNumber(n);
            output = postToJson(map.getPosts(n));
          } else {
            status = "Error: A post from this user ID does not exist";
            break;
          }

          status = "OK";
          break;
        case "comments":
          if (param2.equals("")) {
            status = "Error: Please specify the post to retrieve comments from";
            break;
          }
          n = Character.getNumericValue(param2.charAt(param2.length() - 1));
          if (map.getComments(n) != null) {
            output = postToJson(map.getComments(n));
            entries = map.getCommentNumber(n);
            status = "OK";
          } else {
            status = "Error: A post with this ID does not exist";
          }
          break;

        default:
          status = "Error: Your first parameter was invalid. Please correct this and try again";
          break;
      }

    } else {
      status = "Error: please enter an Integer value for the second parameter";
    }

    String data;
    if (status.equals("OK")) {
      data = JsonBuilder.build(status, entries, output);
    } else {
      data = JsonBuilder.build(status);
    }
    //writer.print(data);
    return data;
  }

  /**
   * Converts a post map into a Json Array Object.
   * @param p A Map(Integer, Post) representing the posts
   * @return A Json format string of the post
   **/
  private String postToJson(Map<Integer, Post> p) {
    ArrayList<String> a = new ArrayList<>();
    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    for (int key : p.keySet()) {
      a.add(gson.toJson(p.get(key)));
    }

    return a.toString();
  }

  /**
   * Entry Point
   * @param args Main parameter
   * @throws IOException throws an IOException form run
   **/
  public static void main(String[] args) throws IOException {
    SimpleServer serv = SimpleServer.getInstance();
    serv.run();
  }


}
