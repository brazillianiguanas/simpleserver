package simpleserver;

import com.google.gson.*;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class DataMap {

  private JsonObject map;
  private Map<Integer, Map<Integer, Post>> posts;
  private Map<Integer, Map<Integer, Post>> comments;

  /**
   * Constructor.
   * @throws IOException Throws an IOException if no data file is found
   **/
  DataMap() throws IOException {

    //Read the Json file and store text in a Json Element
    JsonElement jtext = new JsonParser().parse(new FileReader("data.json.txt"));

    //Create a Json object for the file, can be used to ge all users and posts but not specifics
    map = jtext.getAsJsonObject();
    //System.out.println(map.get("users").toString());

    //Create Json arrays for the Json objects
    JsonArray p = map.get("posts").getAsJsonArray();
    JsonArray c = map.get("comments").getAsJsonArray();

    //Initialize post and comment maps
    posts = new HashMap<Integer, Map<Integer, Post>>();
    comments = new HashMap<Integer, Map<Integer, Post>>();

    Gson gson = new Gson();
    Post temp;

    //first load posts into maps
    for (int i = 0; i < p.size(); i++) {
      temp = gson.fromJson(p.get(i), Post.class);
      posts.putIfAbsent(temp.userid, new HashMap<Integer, Post>());
      posts.get(temp.userid).put(temp.postid, temp);
    }

    System.out.println("posts mapped");

    //then load comments into maps
    for (int i = 0; i < c.size(); i++) {
      temp = gson.fromJson(c.get(i), Post.class);
      comments.putIfAbsent(temp.postid, new HashMap<Integer, Post>());
      comments.get(temp.postid).put(temp.userid, temp);
    }

    System.out.println("comments mapped");


  }

  /**
   * Getter method for UserNumber
   * @return Returns number of users as an int
   **/
  int getUserNumber() {
    return map.get("users").getAsJsonArray().size();
  }

  /**
   * Getter method for Users.
   * @return Returns a JsonElement object containing the users
   **/
  JsonElement getUsers() {
    return map.get("users");
  }


  /**
   * Getter method for PostNumber.
   * @param key An int, the UserID.
   * @return Returns the number
   * of posts of a specific user as an int
   **/
  int getPostNumber(int key) {
    return posts.get(key).size();
  }

  /**
   * Getter method for the posts.
   * @return Returns all posts as a JsonElement
   **/
  public JsonElement getPosts() {
    return map.get("posts");
  }

  /**
   * Getter method for posts of a specific user.
   * @param userID An int userID.
   * @return Returns the
   * specified users posts as a Map(Integer, Post)
   **/
  Map<Integer, Post> getPosts(int userID) {
    return posts.get(userID);
  }

  /**
   * Getter method for number of comments of a particular user.
   * @param key An int UserID
   * @return the number of comments of a specified user as an int
   **/
  int getCommentNumber(int key) {
    return comments.get(key).size();
  }

  /**
   * Getter method for comments of a particular post.
   * @param postID An int PostID as a parameter.
   * @return Returns
   * the specified posts comments as a Map(Integer, Post)
   **/
  Map<Integer, Post> getComments(int postID) {
    return comments.get(postID);
  }


  class Post {

    int postid;
    int userid;
    protected String data;

  }

}
