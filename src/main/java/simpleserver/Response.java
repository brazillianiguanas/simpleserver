package simpleserver;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.Date;
import java.text.SimpleDateFormat;

public class Response {

  private String status;
  private String timestamp;
  private int entries;

  /**
   * Constructor.
   * @param status Status of the response
   **/
  public Response(String status) {
    this.status = status;
  }

  /**
   * Constructor.
   * @param status  Status of the response
   * @param entries The number of entries
   **/
  public Response(String status, int entries) {
    this.status = status;
    this.entries = entries;
    timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
  }


  public static class JsonBuilder {

    /**
     * Builds a response and Json format string.
     * @param status Status of the response
     * @param entries Number of entries
     * @param data Data of the response
     * @return returns A response string in pretty Json format
     **/
    static String build(String status, int entries, String data) {
      JsonParser parser = new JsonParser();
      Gson gson = new GsonBuilder().setPrettyPrinting().create();
      JsonObject g = parser.parse(gson.toJson(new Response(status, entries))).getAsJsonObject();
      g.add("data", parser.parse(data).getAsJsonArray());
      return gson.toJson(g);
    }

    /**
     * Builds a response and Json format string.
     * @param status Status of the response
     * @return returns A response string in pretty Json format
     **/
    static String build(String status) {
      Gson gson = new GsonBuilder().setPrettyPrinting().create();

      return gson.toJson(new Response(status));
    }

    /**
     * creates a pretty Json response from a Json string.
     * @param jsonString String representing the Json string
     * @return Returns the Json string in pretty format
     **/
    static String toPrettyFormat(String jsonString) {
      JsonParser parser = new JsonParser();
      JsonObject json = parser.parse(jsonString).getAsJsonObject();

      Gson gson = new GsonBuilder().setPrettyPrinting().create();
      String prettyJson = gson.toJson(json);

      return prettyJson;
    }

  }
}
